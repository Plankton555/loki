# README #

Loki is a StarCraft AI that is written in Java and uses [BWMirror](http://bwmirror.jurenka.sk/) to play the game.

### How does this work? ###

The bot uses [BWMirror](http://bwmirror.jurenka.sk/) to connect to [the Brood War API (BWAPI)](https://github.com/bwapi/bwapi), which in turn injects a dll file into the StarCraft process to interact with the game. BWAPI is constructed to only allow the AI to interact with the game in the same way as a human player does (kinda... more details about how it works can be found [here](http://bwapi.github.io/index.html#project)).

### How do I set this up? ###

**The following is adapted from [the SSCAI tournament's tutorial for getting started](http://sscaitournament.com/index.php?action=tutorial) and regards this specific project:**

The Loki project assumes you have the following components:

* Starcraft (v. 1.16.1)
* Windows (Mac and Linux are unsupported)
* Java 1.7 (64-bit is not supported, so you need a 32-bit JRE)
* BWAPI 3.7.4 [(available here)](https://bwapi.googlecode.com/files/BWAPI%203.7.4.7z) (note that BWAPI 4.0 will most likely not work)
* Chaoslauncher (included with BWAPI)
* BWMirror 1.2 (included in the project files)

Once you have all of the above, follow these steps to get the project up and running:

1. Install Starcraft and Brood War expansion.
2. Update to latest version (1.16.1).
3. Download BWAPI, extract files from zip.
4. Run install.exe to install BWAPI.
5. Edit <Starcraft dir>/bwapi-data/bwapi.ini:
	* Change line 8 "ai = <path-to-dll-file>" to "ai = NULL".
	* Remove (or comment with ;) line 9 "ai_dbg = <path-to-dll-file>".
6. Clone this repo (Loki) if you haven't done so already.
	* The next step is to set the project up in an IDE (such as Eclipse or IntelliJ)
7. Run the IDE as an Administrator.
8. Import the project by selecting the <path-to-repo> folder (or the .project file if you're using IntelliJ).
9. If you are using IntelliJ, you might have to change the Project Language Level to a number higher than 6:
	* Select the Project and navigate to "File->Project Structure". Change Project Language Level to a number higher than 6 (8 works for me).
10. Change the Run/Debug Configuration and set the Working Directory to <path-to-repo>/release (instead of just <path-to-repo>).
11. Right-click on Loki (located in src/java/loki), select Run As and select Java Application.
	* Expected output: "Attempting to init BWAPI..." "BWAPI ready." "Connecting to Broodwar..."
12. Start Chaoslauncher (included in the BWAPI files) as an Administrator and activate the BWAPI Release plugin.
13. Start the Starcraft game using Chaoslauncher.
	* Expected output: "Connected" "Connection successful, starting match..." "Waiting..."
14. Begin a custom game as Zerg (the race that Loki is designed to play).
15. The BWAPI player should take control of the player's units and begin ordering units.
	* Note: the game will freeze for one or two minutes the first time a new map is played as [BWTA](https://code.google.com/p/bwta/) has to analyze the map.

**If you encounter a problem, have a look at the [troubleshooting section](http://sscaitournament.com/index.php?action=tutorial#faq) of the SSCAI tournament's tutorial.**