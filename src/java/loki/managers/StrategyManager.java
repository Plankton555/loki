package loki.managers;

import bwapi.Mirror;
import bwapi.UnitType;
import loki.CommunicationChannel;
import loki.data.BuildOrder;
import loki.data.BuildOrderItem;
import loki.data.UnitInventory;
import loki.utilitysystem.AttackBehaviour;
import loki.utilitysystem.DefendBehaviour;
import loki.utilitysystem.EnemySearchBehaviour;
import loki.utilitysystem.ExpandBehaviour;
import loki.utilitysystem.UtilitySystem;


/**
 * Created by Plankton on 2015-02-13.
 */
public class StrategyManager {

    private Mirror bwapi;
    private CommunicationChannel commChannel;
    private UnitInventory unitInventory;
    private UtilitySystem utilitySystem;

    public StrategyManager(Mirror bwapi, CommunicationChannel commChannel, UnitInventory unitInventory) {
        this.bwapi = bwapi;
        this.commChannel = commChannel;
        this.unitInventory = unitInventory;
    }

    public void update() {
        int updateInterval = 1;
        if (bwapi.getGame().getFrameCount() % updateInterval != 0) { //only update at a certain interval
            return;
        }
        if (bwapi.getGame().getFrameCount() % 64 == 0) {
            utilitySystem.tick();
        }

        BuildOrder bo = commChannel.getProductionManager().getBuildOrder();
        if (bo.getNrOfStatus(BuildOrderItem.BuildOrderStatus.NOT_STARTED) < 5) {
            renewBuildOrder(bo);
        }
    }

    private void renewBuildOrder(BuildOrder bo) {
        bo.addToBuildOrder(new BuildOrderItem(UnitType.Zerg_Zergling), 8);
        bo.addToBuildOrder(new BuildOrderItem(UnitType.Zerg_Drone), 2);
        if (bwapi.getGame().self().minerals() > 600) {
            bo.addToBuildOrder(new BuildOrderItem(UnitType.Zerg_Hatchery));
        }
    }

    public void initialize() {
        initializeBuildOrder();
        initializeUtilitySystem();
    }

    private void initializeUtilitySystem() {
        utilitySystem = new UtilitySystem();
        utilitySystem.addBehaviour(new AttackBehaviour(commChannel));
        utilitySystem.addBehaviour(new DefendBehaviour(commChannel));
        utilitySystem.addBehaviour(new ExpandBehaviour(commChannel));
        utilitySystem.addBehaviour(new EnemySearchBehaviour(commChannel));
    }

    private void initializeBuildOrder() {
        BuildOrder bo = commChannel.getProductionManager().getBuildOrder();
        bo.addToBuildOrder(new BuildOrderItem(UnitType.Zerg_Drone), 4);
        bo.addToBuildOrder(new BuildOrderItem(UnitType.Zerg_Overlord));
        //bo.addToBuildOrder(new BuildOrderItem(UnitType.Zerg_Extractor));
        bo.addToBuildOrder(new BuildOrderItem(UnitType.Zerg_Drone), 4);
        bo.addToBuildOrder(new BuildOrderItem(UnitType.Zerg_Spawning_Pool));
        bo.addToBuildOrder(new BuildOrderItem(UnitType.Zerg_Drone), 4);
        bo.addToBuildOrder(new BuildOrderItem(UnitType.Zerg_Hatchery));
        bo.addToBuildOrder(new BuildOrderItem(UnitType.Zerg_Zergling), 3);
        bo.addToBuildOrder(new BuildOrderItem(UnitType.Zerg_Overlord));
        bo.addToBuildOrder(new BuildOrderItem(UnitType.Zerg_Zergling));
        bo.addToBuildOrder(new BuildOrderItem(UnitType.Zerg_Drone), 3);
        bo.addToBuildOrder(new BuildOrderItem(UnitType.Zerg_Zergling), 5);
        bo.addToBuildOrder(new BuildOrderItem(UnitType.Zerg_Hatchery));
    }

    public UtilitySystem getUtilitySystem() {
        return utilitySystem;
    }
}
