package loki.managers;

import java.util.LinkedList;
import java.util.List;

import bwapi.Mirror;
import bwapi.Position;
import bwapi.UnitType;
import bwta.BWTA;
import bwta.Region;
import loki.CommunicationChannel;
import loki.data.UnitData;

/**
 * Created by Plankton on 2014-12-13.
 */
public class ScoutingManager extends AbstractEmployer {

    private Mirror bwapi;
    private CommunicationChannel commChannel;

    public ScoutingManager(Mirror bwapi, CommunicationChannel commChannel) {
        attachedUnits = new LinkedList<>();
        this.bwapi = bwapi;
        this.commChannel = commChannel;
    }

    public void update() {
        super.update();
        for (UnitData u : commChannel.getUnitInventory().getUnassignedUnits()) {
            if (u.getType() == UnitType.Zerg_Overlord) {
                attachUnit(u);
            }
        }
        for (UnitData u : attachedUnits) {
            doScouting(u);
        }
    }

    private void doScouting(UnitData u) {
        if (u.getUnit().isMoving()) {
            return;
        }
        // Move to a position in a random region (very basic scouting)
        List<Region> regions = BWTA.getRegions();
        //System.out.println("nrRegions: " + regions.size());
        int nrRegions = regions.size();
        Region pickedRegion = regions.get((int) (Math.random() * nrRegions));
        Position p = pickedRegion.getCenter();
        u.getUnit().move(p, true);
    }
}
