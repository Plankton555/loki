package loki.managers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import bwapi.Mirror;
import bwapi.Position;
import bwta.BWTA;
import bwta.BaseLocation;
import bwta.Region;
import loki.CommunicationChannel;
import loki.data.UnitData;
import loki.data.UnitInventory;
import loki.utilitysystem.AttackBehaviour;
import loki.utilitysystem.EnemySearchBehaviour;
import loki.utilitysystem.ExpandBehaviour;
import loki.utilitysystem.IBehaviour;
import loki.utilitysystem.UtilitySystem;

/**
 * Created by Plankton on 2015-04-03.
 */
public class ArmyManager extends AbstractEmployer {

	private Mirror bwapi;
	private CommunicationChannel commChannel;
	private UnitInventory unitInventory;

	public ArmyManager(Mirror bwapi, CommunicationChannel commChannel, UnitInventory unitInventory) {
		this.bwapi = bwapi;
		this.commChannel = commChannel;
		this.unitInventory = unitInventory;
	}

	public void update() {
		super.update();
		refreshAttachedUnits();

		if (bwapi.getGame().getFrameCount() % 32 == 0) {
			UtilitySystem us = commChannel.getStrategyManager().getUtilitySystem();
			// System.out.println("ArmyManager.attachedUnits.size(): " +
			// attachedUnits.size());

			IBehaviour tb = us.getTopBehaviour();
			if (tb instanceof AttackBehaviour || tb instanceof ExpandBehaviour) {
				performAttackBehaviour();
			} else {
				for (IBehaviour b : us.getBehaviours()) {
					if (b instanceof EnemySearchBehaviour) {
						if (b.getUtility() > 0.1) {
							performSearchForEnemy();
						}
					}
				}
			}
		}
	}

	private void performSearchForEnemy() {
		for (UnitData u : attachedUnits) {
			doSearchForEnemy(u);
		}
	}

	private void performAttackBehaviour() {
		Collection<UnitData> enemies = unitInventory.getEnemyUnits();
		boolean knowsAboutEnemy = !enemies.isEmpty();
		UnitData target = null;
		if (knowsAboutEnemy) {
			target = findTarget(enemies);
		}

		// find "pack leader" (the one closest to all others)
		double closestDistance = Double.MAX_VALUE;
		UnitData packLeader = null;
		for (UnitData u1 : attachedUnits) {
			double summedDistance = 0;
			for (UnitData u2 : attachedUnits) {
				if (u1.getUnitID() != u2.getUnitID()) { // not same unit
					summedDistance += u1.getDistance(u2);
					if (summedDistance > closestDistance) {
						break; // no need to consider this unit anymore
					}
				}
			}

			if (summedDistance < closestDistance) {
				closestDistance = summedDistance;
				packLeader = u1;
			}
		}

		for (UnitData u : attachedUnits) {
			if (target != null) {
				// if knows about enemy
				doAttack(u, target, packLeader);
			} else {
				// start searching for enemies again
				for (IBehaviour b : commChannel.getStrategyManager().getUtilitySystem().getBehaviours()) {
					if (b instanceof EnemySearchBehaviour) {
						((EnemySearchBehaviour) b).setUtility(1);
						break;
					}
				}
			}
		}
	}

	private UnitData findTarget(Collection<UnitData> enemies) {
		// find closest enemy and attack it
		UnitData target = null;

		UnitData baseBuilding = null;
		UnitData building = null;
		UnitData randomUnit = null;
		int randomUnitIndex = (int) (Math.random() * enemies.size());
		int counter = 0;
		for (UnitData e : enemies) {
			if (e.getType().isResourceDepot()) { // base building
				baseBuilding = e;
				break;
			} else if (e.getType().isBuilding()) {
				building = e;
			}
			if (counter == randomUnitIndex) {
				randomUnit = e;
			}
		}

		if (baseBuilding != null || building != null) {
			target = (baseBuilding != null) ? baseBuilding : building;
		} else {
			target = randomUnit;
		}
		return target;
	}

	private void refreshAttachedUnits() {
		for (UnitData u : unitInventory.getUnassignedUnits()) {
			if (u.isMilitary()) {
				attachUnit(u);
			}
		}

		/*
		 * for (int i = 0; i < attachedUnits.size(); i++) { if
		 * (!attachedUnits.get(i).exists()) { attachedUnits.remove(i); } }
		 */
	}

	private void doAttack(UnitData u, UnitData target, UnitData packLeader) {
		if (u.isAttacking() || u.isAttackFrame() || u.isStartingAttack()) {
			return;
		}
		double gatheringDistance = 300;
		if (packLeader != null && u.getDistance(packLeader) > gatheringDistance) {
			u.attack(packLeader.getPosition()); // regroup
			return;
		}

		if (target != null) {
			u.attack(target.getPosition());
			return;
		}
	}

	private void doSearchForEnemy(UnitData u) {
		if (u.isMoving() || u.isAttacking() || u.isAttackFrame() || u.isStartingAttack()) {
			return;
		}
		// Move to a random unscouted region
		List<Region> regions = BWTA.getRegions();
		List<Position> unexploredPositions = new ArrayList<Position>();
		List<Position> exploredPositions = new ArrayList<Position>();
		for (Region r : regions) {
			for (BaseLocation b : r.getBaseLocations()) {
				if (!bwapi.getGame().isExplored(b.getTilePosition())) {
					unexploredPositions.add(b.getPosition());
				} else {
					exploredPositions.add(b.getPosition());
				}
			}
		}
		// System.out.println("nrRegions: " + regions.size());
		int nrUnexploredPositions = unexploredPositions.size();
		int nrExploredPositions = exploredPositions.size();
		Position p = null;
		if (nrUnexploredPositions > 0) {
			p = unexploredPositions.get((int) (Math.random() * nrUnexploredPositions));
		} else {
			p = exploredPositions.get((int) (Math.random() * nrExploredPositions));
		}
		u.move(p, false);
	}
}
