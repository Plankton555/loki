package loki.managers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import bwapi.Mirror;
import bwapi.TilePosition;
import bwapi.Unit;
import bwapi.UnitType;
import bwta.BWTA;
import bwta.BaseLocation;
import loki.CommunicationChannel;
import loki.data.BuildOrder;
import loki.data.BuildOrderItem;
import loki.data.BuildOrderItem.BuildOrderStatus;
import loki.data.UnitData;
import loki.data.UnitInventory;
import loki.util.Utils;
import loki.utilitysystem.ExpandBehaviour;
import loki.utilitysystem.IBehaviour;

/**
 * Created by Plankton on 2014-09-06.
 */
public class ProductionManager {

    private Mirror bwapi;
    private CommunicationChannel commChannel;
    private List<Base> bases;
    private UnitInventory unitInventory;
    private BuildOrder buildOrder;
	private boolean isExpanding;
	private BuildOrderItem expansionBOItem;

    public ProductionManager(Mirror bwapi, CommunicationChannel commChannel, UnitInventory unitInventory) {
        this.bwapi = bwapi;
        this.commChannel = commChannel;
        this.unitInventory = unitInventory;
        bases = new ArrayList<Base>();

        buildOrder = new BuildOrder();
    }

    public void update() {
    	for (int i=0; i<bases.size(); i++) {
    		Base currentBase = bases.get(i);
    		if (!currentBase.getBaseBuilding().exists()) {
    			// Base building is destroyed
    			// unassign units and remove Base object
    			currentBase.getWorkers().detachAllUnits();
        		bases.remove(i);
        		i--;
    		}
    	}
        if (bases.isEmpty()) {
            // we do not have a base... try to find a new one...
            Collection<UnitData> baseBuildings = unitInventory.getOwnUnits(UnitType.Zerg_Hatchery); // TODO must also include Lair and Hive
            if (baseBuildings.size() > 0) {
                UnitData baseBuilding = baseBuildings.iterator().next();
                //System.out.println("Found base building");
                bases.add(new Base(bwapi, baseBuilding, commChannel));
            } else {
                // not good, can't find any base building
                System.out.println("Unable to find a base building");
            }
        }
        
        doExpansionBehaviour();

        for (UnitData u : unitInventory.getUnassignedUnits()) {
            if (u.getType().isWorker()) {
                assignToNeedingBase(u);
            }
        }
        distributeWorkers();

        for (Base base : bases) {
            base.update();
        }

        if (bwapi.getGame().getFrameCount() % 20 == 0) {
            executeBuildOrder();
        }

        checkSupply();
    }
    
    private void doExpansionBehaviour() {
    	if (isExpanding) {
    		if (expansionBOItem != null) {
            	// if the expansion is underway
        		if (expansionBOItem.status == BuildOrderStatus.COMPLETED) {
        			// this should mean that the base is finished, set it as new base
        			System.out.println("New base completed!");
        			if (expansionBOItem.associatedUnit.getType() != UnitType.Zerg_Hatchery) {
        				System.out.println("Expansion is not a hatchery! :O");
        			}
        			bases.add(new Base(bwapi, expansionBOItem.associatedUnit, commChannel));

            		isExpanding = false;
            		expansionBOItem = null;
            		for (IBehaviour b : commChannel.getStrategyManager().getUtilitySystem().getBehaviours()) {
            			if (b instanceof ExpandBehaviour) {
            				((ExpandBehaviour) b).setUtility(0);
            			}
            		}
        		}
        	} else if (expansionBOItem == null) {
        		// find a base position and build a hatchery there
        		System.out.println("Finding location to expand to");
        		List<BaseLocation> baseLocations = BWTA.getBaseLocations();
        		BaseLocation closestBaseLocation = null;
        		double minDistance = Double.MAX_VALUE;
        		for (BaseLocation b : baseLocations) {
        			
        			boolean alreadyOccupied = false;
        			for (Base base : bases) {
        				//if (Utils.isSameRegionGroup(b.getPosition(), base.getBaseBuilding().getPosition())) {
        				if (BWTA.getRegion(base.getBaseBuilding().getPosition()).getCenter().equals(b.getRegion().getCenter())) {
        					alreadyOccupied = true;
        					break;
        				}
        			}
        			if (alreadyOccupied) {
        				continue;
        			} else {
        				double dist = BWTA.getGroundDistance(bases.get(0).getBaseBuilding().getTilePosition(), b.getTilePosition());
        				if (dist > 0 && dist < minDistance) {
        					minDistance = dist;
        					closestBaseLocation = b;
        				}
        			}
        		}
        		
        		System.out.println("Adding a new base to build order!");
        		if (closestBaseLocation == null) {
        			System.out.println("   closestBaseLocation was null!");
        		} else {
        			expansionBOItem = new BuildOrderItem(UnitType.Zerg_Hatchery, 10, closestBaseLocation.getTilePosition());
        			buildOrder.assureInBuildOrder(expansionBOItem);
        		}
        		
        	}
        } else {
        	if (commChannel.getStrategyManager().getUtilitySystem().getTopBehaviour() instanceof ExpandBehaviour) {
        		isExpanding = true;
        	}
        }
    }

    private void distributeWorkers() {
    	// redistribute one worker at a time
    	// find base with fewest workers, and with most workers
    	if (bases.size() == 1) {
    		return;
    		// no need to redistribute if only one base
    	}
    	Base baseFewest = null;
    	Base baseMost = null;
    	int fewest = 50;
    	int most = -50;
    	for (Base b : bases) {
    		int relNeeded = b.getRelativeNrNeededWorkers();
    		if (relNeeded > most) {
    			most = relNeeded;
    			baseMost = b;
    		}
    		if (relNeeded < fewest) {
    			fewest = relNeeded;
    			baseFewest = b;
    		}
    	}
    	// if difference is larger than 1, move from more to fewer (since relative, this becomes opposite...)
    	if (Math.abs(most-fewest) > 1 && baseFewest != baseMost) {
    		String message = "Trying to move from base with " + baseFewest.getNrEmployees() + " to base with " + baseMost.getNrEmployees();
    		System.out.println(message);
    		UnitData workerToMove = baseFewest.unAssignWorker();
    		if (workerToMove == null) {
    			System.out.println("worker to transfer was null!");
    		} else {
    			//workerToMove.returnCargo(false);
    			//workerToMove.stop(true);
    			//workerToMove.rightClick(baseMost.getBaseBuilding(), true);
    			//workerToMove.stop();
    			workerToMove.move(baseMost.getBaseBuilding().getPosition());
        		baseMost.assignWorker(workerToMove);
    		}
    	}
    }

    private void executeBuildOrder() {
        int nonReservedMinerals = bwapi.getGame().self().minerals();
        int nonReservedGas = bwapi.getGame().self().gas();
        int nonReservedSupply = Math.max(bwapi.getGame().self().supplyTotal() - bwapi.getGame().self().supplyUsed(), 0);

        //System.out.println("Available: m=" + nonReservedMinerals + ", g=" + nonReservedGas + ", s=" + nonReservedSupply);

        BuildOrder bo = commChannel.getProductionManager().getBuildOrder();
        cleanBuildOrder(bo);
        for (BuildOrderItem b : bo.getBuildOrderItems()) {
            if (b.status == BuildOrderItem.BuildOrderStatus.ONGOING) {
                continue;
            }
            UnitType targetUnitType = b.unitType;
            // TODO must check that can build unit, if not, add prerequisites to build order?
            if (nonReservedMinerals >= targetUnitType.mineralPrice()
                    && nonReservedGas >= targetUnitType.gasPrice()
                    && nonReservedSupply >= targetUnitType.supplyRequired()) {
                // only run this if enough resources and supply

                if (targetUnitType.isBuilding()) {
                    // Find a drone to morph building
                    // TODO some buildings are exceptions!!!
                    for (UnitData ud : unitInventory.getOwnUnits()) {
                        if (ud.getType() == UnitType.Zerg_Drone) {
                            // TODO must check that unit is not already tasked with something important
                            TilePosition p = null;
                            if (b.preferredPosition == null) {
                            	p = getBuildTile(ud.getUnit(), targetUnitType, bwapi.getGame().self().getStartLocation());
                            } else {
                            	p = b.preferredPosition;
                            }
                            
                            if (p != null) {
                                boolean succeeded = ud.build(p, targetUnitType);
                                if (succeeded) {
                                    b.status = BuildOrderItem.BuildOrderStatus.ONGOING;
                                    b.associatedUnit = ud;
                                    if (targetUnitType.isRefinery()) {
                                        for (Unit n : bwapi.getGame().getNeutralUnits()) {
                                            if (n.getType() == UnitType.Resource_Vespene_Geyser
                                                    && n.getTilePosition().equals(p)) {
                                                // found the correct geyser
                                                b.associatedGeyser = n;
                                                break;
                                            }
                                        }
                                    }
                                    //System.out.println("Building a " + targetUnitType.toString());
                                }
                            }
                            break;
                        }
                    }
                } else {
                    // Find a larva to morph unit
                    // TODO some units are exceptions!!!
                    for (UnitData ud : unitInventory.getOwnUnits()) {
                        if (ud.getType() == UnitType.Zerg_Larva) {
                            boolean succeeded = ud.morph(targetUnitType);
                            if (succeeded) {
                                b.status = BuildOrderItem.BuildOrderStatus.ONGOING;
                                b.associatedUnit = ud;
                                //System.out.println("Building a " + targetUnitType.toString());
                            }
                            break;
                        }
                    }
                }
            }

            // reserve resources and supply before processing next build order item
            nonReservedMinerals -= targetUnitType.mineralPrice();
            nonReservedGas -= targetUnitType.gasPrice();
            nonReservedSupply -= targetUnitType.supplyRequired();
            //System.out.println("Non-reserved: m=" + nonReservedMinerals + ", g=" + nonReservedGas + ", s=" + nonReservedSupply);
        }
    }

    private void cleanBuildOrder(BuildOrder buildOrder) {
        // These two steps could be done in a single loop

        // Loop through each item, if it's finished, set to completed
        for (BuildOrderItem b : buildOrder.getBuildOrderItems()) {
            if (b.status == BuildOrderItem.BuildOrderStatus.ONGOING) {
                if (b.unitType.isRefinery()) {
                    if (b.associatedGeyser != null && !b.associatedGeyser.isMorphing() && !b.associatedUnit.movingToBuildSite()) {
                        if (b.unitType == b.associatedGeyser.getType()) {
                            // Refinery completed
                            b.status = BuildOrderItem.BuildOrderStatus.COMPLETED;
                        } else {
                            // Refinery not completed and not ongoing (by some reason)
                            b.status = BuildOrderItem.BuildOrderStatus.NOT_STARTED;
                        }
                    }
                } else {
                    if (!b.associatedUnit.isMorphing() && !b.associatedUnit.movingToBuildSite()) {
                        if (b.associatedUnit.getType() == b.unitType) {
                            // Build order item completed
                            b.status = BuildOrderItem.BuildOrderStatus.COMPLETED;
                        } else {
                            // Build order item not completed and not ongoing (by some reason)
                            b.status = BuildOrderItem.BuildOrderStatus.NOT_STARTED;
                        }
                    }
                }
            }
        }

        // Loop through again, remove all completed items from build order
        List<BuildOrderItem> bo = buildOrder.getBuildOrderItems();
        for (int i = 0; i < bo.size(); i++) {
            if (bo.get(i).status == BuildOrderItem.BuildOrderStatus.COMPLETED) {
                bo.remove(i);
                i--;
            }
        }
    }

    private void checkSupply() {
        // add overlord to build order if needed

        int supplyLeft = bwapi.getGame().self().supplyTotal() - bwapi.getGame().self().supplyUsed();
        boolean criticalSupply = bwapi.getGame().self().supplyTotal() < 400 && supplyLeft <= 4;
        if (!criticalSupply) {
            return; //if supply not critical, no need to build overlord
        }

        boolean overlordAlreadyInBuildOrder = false;
        double overlordPrio = 10;
        for (BuildOrderItem i : buildOrder.getBuildOrderItems()) {
            if (i.unitType == UnitType.Zerg_Overlord) {
                i.priority = overlordPrio;
                overlordAlreadyInBuildOrder = true;
                break;
            }
        }
        if (!overlordAlreadyInBuildOrder) {
            buildOrder.addToBuildOrder(new BuildOrderItem(UnitType.Zerg_Overlord, overlordPrio));
        }
        buildOrder.sort();
    }

    private void checkWorkers() {
        // build drones if enough minerals
        if (bwapi.getGame().self().minerals() >= UnitType.Zerg_Drone.mineralPrice()) {
            for (UnitData ud : unitInventory.getOwnUnits()) {
                if (ud.getType() == UnitType.Zerg_Larva) {
                    ud.morph(UnitType.Zerg_Drone);
                    break;
                }
            }
        }
    }

    public BuildOrder getBuildOrder() {
        return buildOrder;
    }



    // Returns a suitable TilePosition to build a given building type near
    // specified TilePosition aroundTile, or null if not found. (builder parameter is our worker)
    public TilePosition getBuildTile(Unit builder, UnitType buildingType, TilePosition aroundTile) {
        TilePosition ret = null;
        int maxDist = 3;
        int stopDist = 40;

        // Refinery, Assimilator, Extractor
        if (buildingType.isRefinery()) {
            for (Unit n : bwapi.getGame().getNeutralUnits()) {
                if ((n.getType() == UnitType.Resource_Vespene_Geyser) &&
                        ( Math.abs(n.getTilePosition().getX() - aroundTile.getX()) < stopDist ) &&
                        ( Math.abs(n.getTilePosition().getY() - aroundTile.getY()) < stopDist )) {
                    return n.getTilePosition();
                }
            }
        }

        while ((maxDist < stopDist) && (ret == null)) {
            for (int i=aroundTile.getX()-maxDist; i<=aroundTile.getX()+maxDist; i++) {
                for (int j=aroundTile.getY()-maxDist; j<=aroundTile.getY()+maxDist; j++) {
                    if (bwapi.getGame().canBuildHere(builder, new TilePosition(i,j), buildingType, false)) {
                        // units that are blocking the tile
                        boolean unitsInWay = false;
                        for (Unit u : bwapi.getGame().getAllUnits()) {
                            if (u.getID() == builder.getID()) {
                                continue;
                            }
                            if ((Math.abs(u.getTilePosition().getX()-i) < 4) && (Math.abs(u.getTilePosition().getY()-j) < 4)) {
                                unitsInWay = true;
                            }
                        }
                        if (!unitsInWay) {
                            return new TilePosition(i,j);
                        }
                        // creep for Zerg
                        if (buildingType.requiresCreep()) {
                            boolean creepMissing = false;
                            for (int k=i; k<=i+buildingType.tileWidth(); k++) {
                                for (int l=j; l<=j+buildingType.tileHeight(); l++) {
                                    if (!bwapi.getGame().hasCreep(new TilePosition(k, l))) {
                                        creepMissing = true;
                                    }
                                    break;
                                }
                            }
                            if (creepMissing) {
                                continue;
                            }
                        }
                    }
                }
            }
            maxDist += 2;
        }

        if (ret == null) {
            bwapi.getGame().printf("Unable to find suitable build position for " + buildingType.toString());
        }
        return ret;
    }

    public void assignToNeedingBase(UnitData worker) {
        Base baseMostInNeed = null;
        int mostNeeded = Integer.MIN_VALUE;
        for (Base base : bases) {
            int needed = base.getRelativeNrNeededWorkers();
            if (needed > mostNeeded) {
                mostNeeded = needed;
                baseMostInNeed = base;
            }
        }
        if (baseMostInNeed != null) {
        	baseMostInNeed.assignWorker(worker);
        }
    }
    
    public List<Base> getBases() {
    	return bases;
    }
}
