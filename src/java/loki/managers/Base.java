package loki.managers;

import bwapi.Mirror;
import loki.CommunicationChannel;
import loki.data.UnitData;
import loki.squads.WorkerSquad;

/**
 * Created by Plankton on 2014-09-06.
 */
public class Base extends AbstractEmployer {

    private double nrWorkersPerResource = 2.5;
    private int nrWorkersWanted;
    private CommunicationChannel commChannel;
    private WorkerSquad workers;
    private UnitData baseBuilding;
    private Mirror bwapi;
    public final int mineralDistanceThreshold = 350;

    public Base(Mirror bwapi, UnitData baseBuilding, CommunicationChannel commChannel) {
        this.commChannel = commChannel;
        workers = new WorkerSquad(bwapi, this);
        this.baseBuilding = baseBuilding;
        this.bwapi = bwapi;
    }

    public void assignWorker(UnitData worker) {
        workers.attachUnit(worker);
    }
    
    public UnitData unAssignWorker() {
    	if (workers.getNrEmployees() <= 1) {
    		System.out.println("Can't unassign worker since <= 1 employees");
    		return null;
    	}
    	UnitData w = workers.attachedUnits.get(0);
    	workers.detachUnit(w);
    	return w;
    }

    public void update() {
        super.update();
        refreshAttachedUnits(bwapi);
        if (bwapi.getGame().getFrameCount() % 100 == 0) {
            // find the number of mineral patches nearby
            int nrMineralPatches = 0;
            for (UnitData minerals : commChannel.getUnitInventory().getNeutralUnits()) {
                if (minerals.getType().isMineralField()) {
                    double distance = baseBuilding.getDistance(minerals);
                    if (distance < mineralDistanceThreshold) {
                        nrMineralPatches++;
                    }
                }
            }

            // update nrWorkersWanted
            nrWorkersWanted = (int) (nrMineralPatches*nrWorkersPerResource);
        }
        workers.update();
    }

    private void refreshAttachedUnits(Mirror bwapi) {
        for (UnitData u : commChannel.getUnitInventory().getUnassignedUnits()) {
            if (u.getType().isBuilding() && (bwapi.getGame().getRegionAt(u.getPosition()).getRegionGroupID() == bwapi.getGame().getRegionAt(this.baseBuilding.getPosition()).getRegionGroupID())) {
                attachUnit(u);
            }
        }
    }

    public int getNrWorkersWanted() {
        return nrWorkersWanted;
    }

    public int getRelativeNrNeededWorkers() {
        return nrWorkersWanted - workers.getNrEmployees();
    }
    
    public UnitData getBaseBuilding() {
    	return baseBuilding;
    }
    
    public WorkerSquad getWorkers() {
    	return workers;
    }
}
