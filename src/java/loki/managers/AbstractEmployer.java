package loki.managers;

import java.util.LinkedList;
import java.util.List;

import loki.data.UnitData;

/**
 * Created by Plankton on 2015-06-21.
 */
public abstract class AbstractEmployer {
	// TODO add javadoc
	
    protected List<UnitData> attachedUnits;
    protected List<UnitData> toBeRemoved;

    public AbstractEmployer() {
        attachedUnits = new LinkedList<UnitData>();
        toBeRemoved = new LinkedList<UnitData>();
    }

    public void update() {
        for (UnitData unit : attachedUnits) {
            if (!unit.exists()) {
                detachUnit(unit);
            }
        }
        for (UnitData unit : toBeRemoved) {
            attachedUnits.remove(unit);
        }
        toBeRemoved.clear();
    }

    public void attachUnit(UnitData unit) {
        if (!attachedUnits.contains(unit)) {
            attachedUnits.add(unit);
            unit.setEmployer(this);
            unit.getUnit().stop(true);
        } else {
        	System.out.println("Tried to attach an already attached unit");
        }
    }

    public void detachUnit(UnitData unit) {
        if (attachedUnits.contains(unit)) {
            toBeRemoved.add(unit);
            unit.setEmployer(null);
        }
    }

    public int getNrEmployees() {
        return attachedUnits.size();
    }
    
    public void detachAllUnits() {
    	for (UnitData unit : attachedUnits) {
    		toBeRemoved.add(unit);
    		unit.setEmployer(null);
    	}
    }
}
