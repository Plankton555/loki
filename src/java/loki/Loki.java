package loki;

import bwapi.DefaultBWListener;
import bwapi.Game;
import bwapi.Mirror;
import bwapi.Player;
import bwapi.Position;
import bwapi.Unit;
import bwta.BWTA;
import loki.data.UnitInventory;
import loki.managers.ArmyManager;
import loki.managers.ProductionManager;
import loki.managers.ScoutingManager;
import loki.managers.StrategyManager;
import loki.util.DataMiner;
import loki.util.DebugOverlay;
import loki.util.Utils;

/**
 * Main class for BWAPI bot.
 * @author Plankton (2014-08-27)
 *
 */
public class Loki extends DefaultBWListener {
    private static Loki INSTANCE;
    private Mirror mirror;
    private StrategyManager strategyManager;
    private ProductionManager productionManager;
    private ArmyManager armyManager;
    private ScoutingManager scoutingManager;
    private UnitInventory unitInventory;
    private CommunicationChannel commChannel;
    
    private DataMiner dataMiner;

    private DebugOverlay debugOverlay;

    public static void main(String[] args) {
        new Loki();
    }

    private Loki() {
        INSTANCE = this;
        this.mirror = new Mirror();
        this.mirror.getModule().setEventListener(this);
        this.mirror.startGame();
    }

    /**
     * @return Instance of Loki class. If it is not initialised, an exception is thrown.
     */
    public static Loki getInstance() {
        if (INSTANCE == null) {
            throw new IllegalStateException("Loki is not yet initialized");
        }
        return INSTANCE;
    }

    /**
     * @return Game class.
     */
    public Game getGame() {
        return mirror.getGame();
    }

    /**
     * @return Communication channel.
     */
    public CommunicationChannel getCommChannel() {
        return commChannel;
    }

    @Override
    public void onStart() {
    	Utils.setDiscoveredEnemyRace(null);
    	
        System.out.println("Analyzing map...");
        BWTA.readMap();
        BWTA.analyze();
        System.out.println("Map data ready");

        mirror.getGame().setLocalSpeed(10);
        commChannel = new CommunicationChannel();
        unitInventory = new UnitInventory(mirror);
        strategyManager = new StrategyManager(mirror, commChannel, unitInventory);
        productionManager = new ProductionManager(mirror, commChannel, unitInventory);
        armyManager = new ArmyManager(mirror, commChannel, unitInventory);
        scoutingManager = new ScoutingManager(mirror, commChannel);

        debugOverlay = new DebugOverlay();

        setupCommChannel();

        strategyManager.initialize();

        this.dataMiner = new DataMiner(mirror);
        dataMiner.onStart();
    }

    private void setupCommChannel() {
        commChannel.setUnitInventory(unitInventory);
        commChannel.setStrategyManager(strategyManager);
        commChannel.setProductionManager(productionManager);
        commChannel.setArmyManager(armyManager);
        commChannel.setScoutingManager(scoutingManager);
    }

    @Override
    public void onFrame() {
        unitInventory.update();
        strategyManager.update();
        productionManager.update();
        armyManager.update();
        scoutingManager.update();

        debugOverlay.drawDebugOverlay(mirror, commChannel);
        
        dataMiner.onFrame();
    }

    @Override
    public void onEnd(boolean winner) {
    	dataMiner.onEnd(winner);
    }

    @Override
    public void onSendText(String text) {
    }

    @Override
    public void onReceiveText(Player player, String text) {
    }

    @Override
    public void onNukeDetect(Position p) {
    }

    @Override
    public void onPlayerLeft(Player player) {
    }

    @Override
    public void onUnitCreate(Unit unit) {
        unitInventory.unitCreate(unit);
    }

    @Override
    public void onUnitDestroy(Unit unit) {
        unitInventory.unitDestroy(unit);
    }

    @Override
    public void onUnitDiscover(Unit unit) {
        unitInventory.unitDiscover(unit);
    }

    @Override
    public void onUnitEvade(Unit unit) {
        unitInventory.unitEvade(unit);
    }

    @Override
    public void onUnitHide(Unit unit) {
        unitInventory.unitHide(unit);
    }

    @Override
    public void onUnitMorph(Unit unit) {
        unitInventory.unitMorph(unit);
    }

    @Override
    public void onUnitShow(Unit unit) {
        unitInventory.unitShow(unit);
    }

    @Override
    public void onUnitRenegade(Unit unit) {
        unitInventory.unitRenegade(unit);
    }

    @Override
    public void onSaveGame(String gameName) {
    }

    @Override
    public void onUnitComplete(Unit unit) {
        unitInventory.unitComplete(unit);
    }

    @Override
    public void onPlayerDropped(Player player) {
    }
}