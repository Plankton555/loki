package loki.data;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import loki.data.BuildOrderItem.BuildOrderStatus;

/**
 * Created by Plankton on 2015-02-16.
 */
public class BuildOrder {

    private List<BuildOrderItem> buildOrder;

    /**
     * Initializes the build order.
     */
    public BuildOrder() {
        buildOrder = new LinkedList<BuildOrderItem>();
    }

    /**
     * Adds the specified number of copies of the build order item to the build order. The provided BuildOrderItem is not in itself added to the queue (that is, do not use this method if you need a reference to the object).
     * @param item Build order item to add.
     * @param quantity Number of copies to add.
     */
    public void addToBuildOrder(BuildOrderItem item, int quantity) {
        for (int i = 0; i < quantity; i++) {
            buildOrder.add(new BuildOrderItem(item));
        }
    }

    /**
     * Adds the build order item to the build order.
     * @param item Build order item to add.
     */
    public void addToBuildOrder(BuildOrderItem item) {
        addToBuildOrder(item, 1);
    }

    /**
     * Adds all of the provided build order items to the build order.
     * @param items List of build order items to add.
     */
    public void addToBuildOrder(List<BuildOrderItem> items) {
        for (BuildOrderItem i : items) {
            buildOrder.add(i);
        }
    }

    /**
     * Removes everything from the build order.
     */
    public void clearBuildOrder() {
        buildOrder.clear();
    }

    /**
     * Sorts the build order according to the items' priorities.
     */
    public void sort() {
        Collections.sort(buildOrder, new Comparator<BuildOrderItem>() {
            @Override
            public int compare(BuildOrderItem o1, BuildOrderItem o2) {
                return -Double.compare(o1.priority, o2.priority); // minus because descending
            }
        });
    }

    /**
     * @return List of all uild order items.
     */
    public List<BuildOrderItem> getBuildOrderItems() {
        return buildOrder;
    }

    /**
     * Finds the number of build order items that have the specified status.
     * @param status Status to count.
     * @return Number of build order items.
     */
    public int getNrOfStatus(BuildOrderItem.BuildOrderStatus status) {
        int count = 0;
        for (BuildOrderItem i : buildOrder) {
            if (i.status == status) {
                count++;
            }
        }
        return count;
    }

    /**
     * Makes sure that the specified build order item is in the build order.
     * @param item Build order item.
     */
	public void assureInBuildOrder(BuildOrderItem item) {
		BuildOrderItem toReplace = null;
		for (BuildOrderItem i : buildOrder) {
			if (i.status == BuildOrderStatus.NOT_STARTED && item.unitType == i.unitType && item.priority > i.priority) {
				// make sure that build order contains item of highest prio
				toReplace = i;
				break;
			}
		}
		if (toReplace != null) {
			// TODO might be buggy since the old reference is removed.
			buildOrder.remove(toReplace);
		}
		buildOrder.add(item);
		this.sort();
	}
}
