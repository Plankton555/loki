package loki.data;

import bwapi.TilePosition;
import bwapi.Unit;
import bwapi.UnitType;

/**
 * Created by Plankton on 2015-02-16.
 */
public class BuildOrderItem {

    final public UnitType unitType;
    public double priority;
    public TilePosition preferredPosition;
    public BuildOrderStatus status;
    public UnitData associatedUnit;
    public Unit associatedGeyser; // used for gas refineries

    public enum BuildOrderStatus {
        NOT_STARTED, ONGOING, COMPLETED;
    }
    
    /**
     * Creates BuildOrderItem with specified parameters
     * @param unitType
     * @param priority
     * @param preferredPosition
     */
    public BuildOrderItem(UnitType unitType, double priority, TilePosition preferredPosition) {
    	this.unitType = unitType;
        this.priority = priority;
        this.preferredPosition = preferredPosition;
        this.status = BuildOrderStatus.NOT_STARTED;
        this.associatedUnit = null;
        this.associatedGeyser = null;
    }

    /**
     * Creates a build order item with the specified parameters.
     * @param unitType Unit type.
     * @param preferredPosition Position to build at.
     */
    public BuildOrderItem(UnitType unitType, TilePosition preferredPosition) {
    	this(unitType, 1, preferredPosition);
    }

    /**
     * Creates a build order item with the specified parameters.
     * @param unitType Unit type.
     * @param priority Priority.
     */
    public BuildOrderItem(UnitType unitType, double priority) {
    	this(unitType, priority, null);
    }

    /**
     * Creates a build order item that is a copy of the provided item.
     * @param item Build order item.
     */
    public BuildOrderItem(BuildOrderItem item) {
        this(item.unitType, item.priority, item.preferredPosition);
    }

    /**
     * Creates BuildOrderItem with specified UnitType, while priority and quantity are set to default 1.
     * @param unitType
     */
    public BuildOrderItem(UnitType unitType) {
        this(unitType, 1, null);
    }

    /**
     * Returns the name of the item's unit type.
     */
    public String toString() {
        return unitType.toString();
        //return "prio: " + priority + ", " + unitType.toString();
    }
}
