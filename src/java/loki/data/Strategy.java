package loki.data;

import bwapi.UnitType;

/**
 * Created by Plankton on 2015-02-16.
 */
public class Strategy {

    public static BuildOrder getOpening() {
        BuildOrder buildOrder = new BuildOrder();

        // TODO Currently not used, take a look at this when not too tired
        buildOrder.addToBuildOrder(new BuildOrderItem(UnitType.Zerg_Drone, 4));
        buildOrder.addToBuildOrder(new BuildOrderItem(UnitType.Zerg_Overlord));
        buildOrder.addToBuildOrder(new BuildOrderItem(UnitType.Zerg_Spawning_Pool));

        return buildOrder;
    }

    public static BuildOrder getStrategy() {
        BuildOrder buildOrder = new BuildOrder();

        return buildOrder;
    }
}
