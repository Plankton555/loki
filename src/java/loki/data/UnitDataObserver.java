package loki.data;

/**
 * 
 * @author Plankton (2015-07-03)
 *
 */
public abstract class UnitDataObserver {
    public void onEmployerUpdate(UnitData unit) {}
}
