package loki.data;

import java.util.LinkedList;
import java.util.List;

import bwapi.Player;
import bwapi.Position;
import bwapi.TilePosition;
import bwapi.Unit;
import bwapi.UnitType;
import bwapi.UpgradeType;
import loki.managers.AbstractEmployer;

/**
 * Container/wrapper class for units.
 * @author Plankton (2014-11-04)
 *
 */
public class UnitData {

    private Unit unit;

    private AbstractEmployer employer;
    private List<UnitDataObserver> observers;
    private Position lastKnownPosition;
    private int lastKnownTime;
    private UnitType lastKnownUnitType;
    private boolean isEvaded;
    private int lastKnownHitPoints;

    /**
     * Creates new UnitData from the provided unit. Also initializes list of observers.
     * @param unit Unit connected to this UnitData.
     */
    public UnitData(Unit unit) {
        this.unit = unit;
        this.observers = new LinkedList<UnitDataObserver>();
    }

    /**
     * Adds an observer to this UnitData.
     * @param obs Observer.
     */
    public void addObserver(UnitDataObserver obs) {
        observers.add(obs);
    }

    /**
     * Returns true if the unit is a zerg unit that is morphing.
     * @return true if this unit is currently morphing.
     */
    public boolean isMorphing() {
        return unit.isMorphing();
    }

    /**
     * @return If the unit is currently morphing, returns the unit type that it is morphing to. Otherwise, returns null.
     */
    public UnitType getMorphingTo() {
        return (isMorphing()) ? unit.getBuildType() : null;
    }

    /**
     * @return Unit that this UnitData is associated with.
     */
    public Unit getUnit() {
        return unit;
    }

    /**
     * Returns a unique ID for this unit. It simply casts the unit's address as an integer, since each unit has a unique address.
     * @return Unique ID for this unit.
     */
    public Integer getUnitID() {
        return unit.getID();
    }

    /**
     * Returns the last known unit type of this unit. That is, if it has evaded, the last known unit type is returned. Otherwise, the unit's unit type is returned.
     * @return Returns the last known type of the unit.
     */
    public UnitType getType() {
        if (!isEvaded) {
            return unit.getType();
        } else {
            return lastKnownUnitType;
        }
    }

    /**
     * Orders the unit to morph into the specified unit type. Returns false if given a wrong type.
     * @param morphToType The unit type to morph to.
     * @return True if the morph command succeeded, otherwise false.
     */
    public boolean morph(UnitType morphToType) {
        boolean worked = unit.morph(morphToType);
        return worked;
    }

    /**
     * Returns the last known position of this unit. That is, if it has evaded, the last known position is returned. Otherwise, the unit's position is returned.
     * @return Returns the position of the unit on the map.
     */
    public Position getPosition() {
        if (!isEvaded) {
            return unit.getPosition();
        } else {
            return lastKnownPosition;
        }
    }

    /**
     * Orders the unit to build the given unit type at the given position. Note that if the player does not have enough resources when the unit attempts to place the building down, the order will fail. The tile position specifies where the top left corner of the building will be placed.
     * @param position Target position.
     * @param unitType Target type.
     * @return
     */
    public boolean build(TilePosition position, UnitType unitType) {
        return unit.build(position, unitType);
    }

    /**
     * Returns the build tile position of the unit on the map. Useful if the unit is a building. The tile position is of the top left corner of the building.
     * @return Tile position of the unit.
     */
    public TilePosition getTilePosition() {
        return unit.getTilePosition();
    }

    /**
     * Orders the unit to upgrade the given upgrade type.
     * @param upgradeType Upgrade type.
     * @return True if the upgrade command succeeded, otherwise false.
     */
    public boolean upgrade(UpgradeType upgradeType) {
        return unit.upgrade(upgradeType);
    }

    /**
     * Returns true if the unit is a building that is upgrading. See UpgradeTypes for the complete list of available upgrades in Broodwar.
     * @return True if the unit is a building that is upgrading.
     */
    public boolean isUpgrading() {
        return unit.isUpgrading();
    }

    /**
     * Returns true when a unit has been issued an order to build a structure and is moving to the build location. Also returns true for Terran SCVs while they construct a building.
     * @return True if the unit has been issued a build order.
     */
    public boolean movingToBuildSite() {
        return unit.isConstructing();
    }

    /**
     * Returns the player that owns this unit.
     * @return Player that owns this unit.
     */
    public Player getPlayer() {
        return unit.getPlayer();
    }

    /**
     * Returns the edge-to-edge distance between the current unit and the target unit.
     * @param other Target unit.
     * @return Distance.
     */
    public double getDistance(UnitData other) {
        return unit.getDistance(other.getUnit());
    }

    /**
     * Orders the unit to attack move to the specified location. Takes care of problems with attackFrame().
     * @param target Target position.
     * @return True if the attack command succeeded or if the unit is already attacking.
     */
    public boolean attack(Position target) {
        if (!unit.isAttackFrame()) {
            return unit.attack(target);
        }
        return true;
    }

    /**
     * Orders the unit to attack the specified unit. Takes care of problems with attackFrame().
     * @param target Target unit.
     * @return True if the attack command succeeded or if the unit is already attacking.
     */
    public boolean attack(UnitData target) {
        if (!unit.isAttackFrame()) {
            return unit.attack(target.getUnit());
        }
        return true;
    }

    /**
     * @return True if the unit is counted as a military unit (subject to change!).
     */
    public boolean isMilitary() {
        return (unit.getType() == UnitType.Zerg_Zergling);
    }

    /**
     * @return True if the unit's employer is not null.
     */
    public boolean hasEmployer() {
        return employer != null;
    }

    /**
     * Sets a new employer for this unit. All observers are informed about this change.
     * @param employer
     */
    public void setEmployer(AbstractEmployer employer) {
        this.employer = employer;
        for (UnitDataObserver obs : observers) {
            obs.onEmployerUpdate(this);
        }
    }

    /**
     * @return Employer of this unit.
     */
    public AbstractEmployer getEmployer() {
        return employer;
    }

    /**
     * Orders the unit to move from its current position to the specified position. Also returns false if the unit is in an attack frame.
     * @param position Target position.
     * @param shiftQueueCommand Shift queue command.
     * @return True if the move command succeeded, otherwise false.
     */
    public boolean move(Position position, boolean shiftQueueCommand) {
        if (!unit.isAttackFrame()) {
            return unit.move(position, shiftQueueCommand);
        }
        return false;
    }

    /**
     * Orders the unit to move from its current position to the specified position. Also returns false if the unit is in an attack frame.
     * @param position Target position.
     * @return True if the move command succeeded, otherwise false.
     */
	public boolean move(Position position) {
        if (!unit.isAttackFrame()) {
            return unit.move(position);
        }
        return false;
	}

	/**
	 * Returns true if the unit is moving.
	 * @return True if the unit is moving.
	 */
    public boolean isMoving() {
        return unit.isMoving();
    }

    /**
     * @return True if the unit is in an attack frame.
     */
    public boolean isAttackFrame() {
        return unit.isAttackFrame();
    }

    /**
     * @return True it the unit is currently attacking.
     */
    public boolean isAttacking() {
        return unit.isAttacking();
    }

    /**
     * Returns true if the unit is starting to attack.
     * @return True if the unit is starting to attack.
     */
    public boolean isStartingAttack() {
        return unit.isStartingAttack();
    }

    /**
     * @return True if the unit is guaranteed to exist in the game.
     */
    public boolean exists() {
        return unit.exists();
    }

    /**
     * Marks this unit as evaded.
     * @param frameCount Frame count at the time the unit evaded.
     */
    public void markEvaded(int frameCount) {
        lastKnownPosition = unit.getPosition();
        lastKnownTime = frameCount;
        lastKnownUnitType = unit.getType();
        lastKnownHitPoints = unit.getHitPoints();
        isEvaded = true;
    }

    /**
     * Marks this unit as discovered.
     */
    public void markDiscovered() {
        isEvaded = false;
    }

    /**
     * @return True if this unit is marked as evaded.
     */
    public boolean isEvaded() {
        return isEvaded;
    }

    /**
     * @return If the unit has evaded, returns the last known hit points. Otherwise, returns the unit's hit points.
     */
    public double getHitPoints() {
        if (!isEvaded) {
            return unit.getHitPoints();
        } else {
            return lastKnownHitPoints;
        }
    }

    /**
     * Returns true if the unit is not doing anything.
     * @return True if the unit is not doing anything.
     */
    public boolean isIdle() {
        return unit.isIdle();
    }

    /**
     * Works like the right click in the GUI. Right click on a mineral patch to order a worker to mine, right click on an enemy to attack it.
     * @param otherUnit Target unit.
     * @param shiftQueueCommand Shift queue command.
     * @return True uf the right-click command succeeded.
     */
    public boolean rightClick(UnitData otherUnit, boolean shiftQueueCommand) {
        return unit.rightClick(otherUnit.unit, shiftQueueCommand);
    }

    /**
     * Orders the unit to stop.
     * @return True if the stop command succeeded.
     */
	public boolean stop() {
		return unit.stop();
	}

	/**
	 * Orders the unit to stop.
	 * @param shiftQueueCommand Shift queue command.
	 * @return True if the stop command succeeded.
	 */
	public boolean stop(boolean shiftQueueCommand) {
		return unit.stop(shiftQueueCommand);
	}

	/**
	 * Orders the unit to return its cargo to a nearby resource depot such as a Command Center. Only workers that are carrying minerals or gas can be ordered to return cargo.
	 * @param shiftQueueCommand Shift queue command.
	 * @return True if the return-cargo command succeeded.
	 */
	public boolean returnCargo(boolean shiftQueueCommand) {
		return unit.returnCargo(shiftQueueCommand);
	}
}
