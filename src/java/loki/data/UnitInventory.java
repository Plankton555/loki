package loki.data;

import java.util.Collection;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import bwapi.Mirror;
import bwapi.Player;
import bwapi.Unit;
import bwapi.UnitType;
import loki.util.Utils;

/**
 * Class for managing information about all known units.
 * @author Plankton (2014-11-04)
 *
 */
public class UnitInventory extends UnitDataObserver {
    private static boolean DEBUG_PRINT = false;

    private Map<Integer, UnitData> ownUnitsMap;
    private Map<Integer, UnitData> enemyUnitsMap;
    private Map<Integer, UnitData> neutralUnitsMap;
    private List<UnitData> unassignedUnits;
    private Mirror bwapi;

    public UnitInventory(Mirror bwapi) {
        this.ownUnitsMap = new Hashtable<Integer, UnitData>();
        this.enemyUnitsMap = new Hashtable<Integer, UnitData>();
        this.neutralUnitsMap = new Hashtable<Integer, UnitData>();
        this.unassignedUnits = new LinkedList<UnitData>();
        this.bwapi = bwapi;
    }

    public void update() {
        int updateInterval = 32;
        if (bwapi.getGame().getFrameCount() % updateInterval == 0) { //only update at a certain interval
            findDiscrepancies();
        }
        clearAssignedUnits();
    }

    private void clearAssignedUnits() {
        List<UnitData> assigned = new LinkedList<UnitData>();
        for (UnitData u : unassignedUnits) {
            if (!u.exists() || u.hasEmployer()) {
                assigned.add(u);
            }
        }
        unassignedUnits.removeAll(assigned);
    }

    public List<UnitData> getUnassignedUnits() {
        List<UnitData> output = new LinkedList<UnitData>();
        for (UnitData u : unassignedUnits) {
            output.add(u);
        }
        return output;
    }

    private void findDiscrepancies() {
        //System.out.println("Searching for discrepancies in unit inventory");
        HashSet<Integer> toBeChecked = new HashSet<Integer>(ownUnitsMap.size() + enemyUnitsMap.size() + neutralUnitsMap.size());

        // add everything to toBeCleared first
        for (UnitData ud : ownUnitsMap.values()) {
            toBeChecked.add(ud.getUnitID());
        }
        for (UnitData ud : enemyUnitsMap.values()) {
            if (!ud.isEvaded()) { //keep information about evaded units
                toBeChecked.add(ud.getUnitID());
            }
        }
        for (UnitData ud : neutralUnitsMap.values()) {
            if (!ud.isEvaded()) { //keep information about evaded units
                toBeChecked.add(ud.getUnitID());
            }
        }

        // Find the units we actually know about
        for (Unit unit : bwapi.getGame().getAllUnits()) {
            int uID = unit.getID();

            if (unit.getPlayer().isNeutral()) {
                boolean exists = neutralUnitsMap.containsKey(uID);
                if (exists) {
                    toBeChecked.remove(uID);
                }
                else {
                    //discrepancy
                    System.out.println("  neutral unit " + unit.getType().toString() + " not present in UnitInventory");
                }
            } else if (unit.getPlayer() == bwapi.getGame().self()) {
                boolean exists = ownUnitsMap.containsKey(uID);
                if (exists) {
                    toBeChecked.remove(uID);
                }
                else {
                    //discrepancy
                    System.out.println("  own unit " + unit.getType().toString() + " not present in UnitInventory");
                }
            } else /* if (unit.getPlayer() == bwapi.getGame().enemy())*/ {
                boolean exists = enemyUnitsMap.containsKey(uID);
                if (exists) {
                    toBeChecked.remove(uID);
                }
                else {
                    //discrepancy
                    System.out.println("  enemy unit " + unit.getType().toString() + " not present in UnitInventory");
                }
            }
        }

        // clear all unitdata that was not in the loop above
        for (Integer index : toBeChecked) {
            if (ownUnitsMap.containsKey(index)) {
                UnitData unit = ownUnitsMap.get(index);
                System.out.println("  own unit " + unit.getType().toString() + " present in UnitInventory but not in game");
            } else if (enemyUnitsMap.containsKey(index)) {
                UnitData unit = enemyUnitsMap.get(index);
                System.out.println("  enemy unit " + unit.getType().toString() + " present in UnitInventory but not in game");
            } else if (neutralUnitsMap.containsKey(index)) {
                UnitData unit = neutralUnitsMap.get(index);
                System.out.println("  neutral unit " + unit.getType().toString() + " present in UnitInventory but not in game");
            } else {
                System.out.println("  lolwut???");
            }
        }
    }

    /**
     * Returns true if any self-owned unit is morphing to unitType
     * @param unitType
     * @return
     */
    public boolean isMorphing(UnitType unitType) {
        boolean output = false;
        for (UnitData ud : ownUnitsMap.values()) {
            if (ud.getMorphingTo() == unitType) {
                output = true;
                break;
            }
        }
        return output;
    }

    /**
     * Returns a collection containing all self-owned units stored in UnitInventory
     * @return Collection of all self-owned units
     */
    public Collection<UnitData> getOwnUnits() {
        return ownUnitsMap.values();
    }

    /**
     * BWAPI calls this when an accessible unit is created. Note that this is NOT called when a unit changes
     * type (such as larva into egg or egg into drone). Building a refinery/assimilator/extractor will not
     * produce an onUnitCreate call since the vespene geyser changes to the unit type of the
     * refinery/assimilator/extractor. If the unit is not accessible at the time of creation (i.e. if the
     * unit is invisible and Complete Map Information is disabled), then this callback will NOT be called.
     * If the unit is visible at the time of creation, AIModule::onUnitShow will also be called.
     * @param unit
     */
    public void unitCreate(Unit unit) {
        // just add the unit to the correct unit map (if not already there)
        Player owner = unit.getPlayer();
        String strOwner = "unknown";
        if (owner.isNeutral()) {
            strOwner = "neutral";
            if (!neutralUnitsMap.containsKey(unit.getID())) {
                neutralUnitsMap.put(unit.getID(), new UnitData(unit));
            }
        } else if (owner.equals(bwapi.getGame().self())) {
            strOwner = "self";
            if (!ownUnitsMap.containsKey(unit.getID())) {
                UnitData unitData = new UnitData(unit);
                unitData.addObserver(this);
                ownUnitsMap.put(unit.getID(), new UnitData(unit));
                ownUnitCreated(unitData);
            }
        } else {
            strOwner = "enemy";
            if (!enemyUnitsMap.containsKey(unit.getID())) {
                enemyUnitsMap.put(unit.getID(), new UnitData(unit));
            }
        }
        if (DEBUG_PRINT) {
            System.out.println("Unit created: " + unit.getType().toString() + " owned by " + strOwner);
        }
    }

    private void ownUnitCreated(UnitData unitData) {
        unassignedUnits.add(unitData);
    }

    /**
     * BWAPI calls this when a unit dies or otherwise removed from the game (i.e. a mined out mineral patch).
     * When a Zerg drone becomes an extractor, the Vespene geyser changes to the Zerg Extractor type and the
     * drone is destroyed. If the unit is not accessible at the time of destruction, (i.e. if the unit is
     * invisible and Complete Map Information is disabled), then this callback will NOT be called. If the
     * unit was visible at the time of destruction, AIModule::onUnitHide will also be called.
     * @param unit
     */
    public void unitDestroy(Unit unit) {
        // just removes the unit from the correct unit map
        Player owner = unit.getPlayer();
        String strOwner = "unknown";
        if (owner.isNeutral()) {
            strOwner = "neutral";
            neutralUnitsMap.remove(unit.getID());
        } else if (owner.equals(bwapi.getGame().self())) {
            strOwner = "self";
            ownUnitsMap.remove(unit.getID());
        } else {
            strOwner = "enemy";
            enemyUnitsMap.remove(unit.getID());
        }

        if (DEBUG_PRINT) {
            System.out.println("Unit destroyed: " + unit.getType().toString() + " owned by " + strOwner);
        }
        // TODO When a Zerg drone becomes an extractor, the Vespene geyser changes to the Zerg Extractor type and the drone is destroyed.
    }

    /**
     * BWAPI calls this when a unit becomes accessible. If Complete Map Information is enabled, this will be
     * called at the same time as AIModule::onUnitCreate, otherwise it will be called at the same time
     * as AIModule::onUnitShow.
     * @param unit
     */
    public void unitDiscover(Unit unit) {
        // just add the unit to the correct unit map if not already there
        Player owner = unit.getPlayer();
        String strOwner = "unknown";
        if (owner.isNeutral()) {
            strOwner = "neutral";
            if (!neutralUnitsMap.containsKey(unit.getID())) {
                neutralUnitsMap.put(unit.getID(), new UnitData(unit));
            } else {
                UnitData ud = neutralUnitsMap.get(unit.getID());
                ud.markDiscovered();
            }
        } else if (owner.equals(bwapi.getGame().self())) {
            strOwner = "self";
            if (!ownUnitsMap.containsKey(unit.getID())) {
                ownUnitsMap.put(unit.getID(), new UnitData(unit));
            } else {
                UnitData ud = ownUnitsMap.get(unit.getID());
                ud.markDiscovered();
            }
        } else {
            strOwner = "enemy";
            if (!enemyUnitsMap.containsKey(unit.getID())) {
                enemyUnitsMap.put(unit.getID(), new UnitData(unit));
            } else {
                UnitData ud = enemyUnitsMap.get(unit.getID());
                ud.markDiscovered();
            }
        }
        if (DEBUG_PRINT) {
            System.out.println("Unit discovered: " + unit.getType().toString() + " owned by " + strOwner);
        }
    }

    /**
     * BWAPI calls this right before a unit becomes inaccessible. If Complete Map Information is enabled,
     * this will be called at the same time as AIModule::onUnitDestroy, otherwise it will be called at
     * the same time as AIModule::onUnitHide.
     * @param unit
     */
    public void unitEvade(Unit unit) {
        // mark the unit as evaded and store last known position and last known time
        Player owner = unit.getPlayer();
        String strOwner = "unknown";
        if (owner.isNeutral()) {
            strOwner = "neutral";
            if (neutralUnitsMap.containsKey(unit.getID())) {
                UnitData ud = neutralUnitsMap.get(unit.getID());
                ud.markEvaded(bwapi.getGame().getFrameCount());
            }
        } else if (owner.equals(bwapi.getGame().self())) {
            strOwner = "self";
            if (ownUnitsMap.containsKey(unit.getID())) {
                UnitData ud = ownUnitsMap.get(unit.getID());
                ud.markEvaded(bwapi.getGame().getFrameCount());
            }
        } else {
            strOwner = "enemy";
            if (enemyUnitsMap.containsKey(unit.getID())) {
                UnitData ud = enemyUnitsMap.get(unit.getID());
                ud.markEvaded(bwapi.getGame().getFrameCount());
            }
        }

        if (DEBUG_PRINT) {
            System.out.println("Unit evaded: " + unit.getType().toString() + " owned by " + strOwner);
        }
    }

    /**
     * BWAPI calls this right before a unit becomes invisible. If Complete Map Information is disabled,
     * this also means that the unit is about to become inaccessible.
     * @param unit
     */
    public void unitHide(Unit unit) {
        //  If the unit was visible at the time of destruction, unitHide() will also be called (as well as unitDestroy()).
        if (DEBUG_PRINT) {
            System.out.println("Unit hide: " + unit.getType().toString());
        }
    }

    /**
     * BWAPI calls this when an accessible unit changes type, such as from a Zerg Drone to a Zerg Hatchery,
     * or from a Terran Siege Tank Tank Mode to Terran Siege Tank Siege Mode. This is not called when the
     * type changes to or from UnitTypes::Unknown (which happens when a unit is transitioning to
     * or from inaccessibility).
     * @param unit
     */
    public void unitMorph(Unit unit) {

        if (DEBUG_PRINT) {
            System.out.println("Unit morphed: " + unit.getType().toString());
        }
    }

    /**
     * BWAPI calls this when a unit becomes visible. If Complete Map Information is disabled,
     * this also means that the unit has just become accessible.
     * @param unit
     */
    public void unitShow(Unit unit) {
    	if (Utils.getDiscoveredEnemyRace() == null && unit.getPlayer().getID() == bwapi.getGame().enemy().getID()) {
    		System.out.println("Discovered enemy race: " + unit.getType().getRace().c_str());
    		Utils.setDiscoveredEnemyRace(unit.getType().getRace().c_str());
    	}

        if (DEBUG_PRINT) {
            System.out.println("Unit showed: " + unit.getType().toString());
        }
    }

    /**
     * BWAPI calls this when an accessible unit changes ownership.
     * @param unit
     */
    public void unitRenegade(Unit unit) {
        // move unit from one map to the other
        // find the unit data and remove it from old map
        UnitData ud = null;
        if (neutralUnitsMap.containsKey(unit.getID())) {
            ud = neutralUnitsMap.get(unit.getID());
            neutralUnitsMap.remove(unit.getID());
        } else if (ownUnitsMap.containsKey(unit.getID())) {
            ud = ownUnitsMap.get(unit.getID());
            ownUnitsMap.remove(unit.getID());
        } else if (enemyUnitsMap.containsKey(unit.getID())) {
            ud = enemyUnitsMap.get(unit.getID());
            enemyUnitsMap.remove(unit.getID());
        }

        // insert it in new correct map
        if (ud != null) {
            Player owner = unit.getPlayer();
            if (owner.isNeutral()) {
                if (!neutralUnitsMap.containsKey(unit.getID())) {
                    neutralUnitsMap.put(unit.getID(), ud);
                }
            } else if (owner.equals(bwapi.getGame().self())) {
                if (!ownUnitsMap.containsKey(unit.getID())) {
                    ownUnitsMap.put(unit.getID(), ud);
                }
            } else {
                if (!enemyUnitsMap.containsKey(unit.getID())) {
                    enemyUnitsMap.put(unit.getID(), ud);
                }
            }
        }
        if (DEBUG_PRINT) {
            System.out.println("Unit renegaded: " + unit.getType().toString());
        }
    }

    /**
     * Called when the state of a unit changes from incomplete to complete.
     * @param unit The Unit object representing the Unit that has just finished training or constructing.
     */
    public void unitComplete(Unit unit) {

        if (DEBUG_PRINT) {
            System.out.println("Unit completed: " + unit.getType().toString());
        }
    }

    public Collection<UnitData> getOwnUnits(UnitType unitType) {
        Collection<UnitData> output = new LinkedList<UnitData>();
        for (UnitData u : ownUnitsMap.values()) {
            if (u.getType() == unitType) {
                output.add(u);
            }
        }
        return output;
    }

    public Collection<UnitData> getEnemyUnits() {
        Collection<UnitData> output = new LinkedList<UnitData>();
        for (UnitData u : enemyUnitsMap.values()) {
            output.add(u);
        }
        return output;
    }

    public int getNrOwnUnits(UnitType unitType) {
        int amount = 0;
        for (UnitData ud : ownUnitsMap.values()) {
            if (ud.getType() == unitType) {
                amount++;
            }
        }
        return amount;
    }

    public int getNrEnemyUnits(UnitType unitType) {
        int amount = 0;
        for (UnitData ud : enemyUnitsMap.values()) {
            if (ud.getType() == unitType) {
                amount++;
            }
        }
        return amount;
    }

    public boolean hasUnitSelf(UnitType unitType) {
        boolean answer = false;
        for (UnitData ud : ownUnitsMap.values()) {
            if (ud.getType() == unitType) {
                answer = true;
                break;
            }
        }
        return answer;
    }

    public Collection<UnitData> getNeutralUnits() {
        Collection<UnitData> output = new LinkedList<UnitData>();
        for (UnitData u : neutralUnitsMap.values()) {
            output.add(u);
        }
        return output;
    }

    public void onEmployerUpdate(UnitData unit) {
        if (unit.getEmployer() == null) {
            unassignedUnits.add(unit);
        }
    }
}
