package loki.utilitysystem;

import java.util.Collection;

import loki.CommunicationChannel;
import loki.data.UnitData;

/**
 * Created by Plankton on 2015-05-03.
 */
public class EnemySearchBehaviour extends AbstractBehaviour {
    private CommunicationChannel commChannel;

    public EnemySearchBehaviour(CommunicationChannel commChannel) {
        super(0.1);
        this.commChannel = commChannel;
    }

    @Override
    protected double calculateUtilityValue() {

        Collection<UnitData> enemies = commChannel.getUnitInventory().getEnemyUnits();
        boolean knowsAboutEnemy = !enemies.isEmpty();
        return knowsAboutEnemy ? 0 : 1;
    }

    @Override
    public void onTerminate() {

    }

    public String toString() {
        return "Enemy search";
    }
}
