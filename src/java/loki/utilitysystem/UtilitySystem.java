package loki.utilitysystem;

import java.util.LinkedList;
import java.util.List;

/**
 * A general and reusable utility system.
 * Created by Plankton on 2015-04-19.
 */
public class UtilitySystem {

    private List<IBehaviour> behaviours;
    private IBehaviour activeBehaviour;

    /**
     * Creates a new UtilitySystem
     */
    public UtilitySystem() {
        behaviours = new LinkedList<IBehaviour>();
    }

    /**
     * Adds a behaviour to the list of behaviours in the utility system.
     * @param behaviour
     * @return true if the behaviour was successfully added.
     */
    public boolean addBehaviour(IBehaviour behaviour) {
        return behaviours.add(behaviour);
    }

    /**
     * Removes a behaviour from the list of behaviours in the utility system.
     * @param behaviour
     * @return true if this list contained the specified behaviour and it is now removed.
     */
    public boolean removeBehaviour(IBehaviour behaviour) {
        return behaviours.remove(behaviour);
    }

    /**
     * Sends an update (or tick) signal to all behaviours in the system.
     */
    public void tick() {
        for (IBehaviour b : behaviours) {
            b.tick();
        }

        IBehaviour topBehaviour = getTopBehaviour();
        if (topBehaviour != activeBehaviour) { // new top behaviour
            if (activeBehaviour != null) {
                activeBehaviour.onTerminate();
            }
            activeBehaviour = topBehaviour;
            activeBehaviour.onInitialize();
        }
    }

    /**
     * Returns the behaviour with highest utility.
     * @return
     */
    public IBehaviour getTopBehaviour() {
        double highestUtility = Double.MIN_VALUE;
        IBehaviour topBehaviour = null;
        for (IBehaviour b : behaviours) {
            double utility = b.getUtility();
            if (utility > highestUtility) {
                highestUtility = utility;
                topBehaviour = b;
            }
        }

        return topBehaviour;
    }

    /**
     * Returns the list of all behaviours in the utility system.
     * @return
     */
    public List<IBehaviour> getBehaviours() {
        return behaviours;
    }
}
