package loki.utilitysystem;

import loki.CommunicationChannel;
import loki.managers.Base;

/**
 * Created by Plankton on 2015-05-03.
 */
public class ExpandBehaviour extends AbstractBehaviour {

    private CommunicationChannel commChannel;

    public ExpandBehaviour(CommunicationChannel commChannel) {
        super(0.1);
        this.commChannel = commChannel;
    }

    @Override
    protected double calculateUtilityValue() {
    	
    	int minNeed = 50;
    	double workerThreshold = 10; // if lower than this, need to expand
    	for (Base base : commChannel.getProductionManager().getBases()) {
    		if (base.getRelativeNrNeededWorkers() < minNeed) {
    			minNeed = base.getRelativeNrNeededWorkers();
    		}
    	}
    	minNeed = (minNeed < 0) ? 0 : minNeed;
    	if (minNeed > workerThreshold) {
    		return 0;
    	}
    	return (workerThreshold - minNeed)/workerThreshold;
    }

    @Override
    public void onTerminate() {

    }

    public String toString() {
        return "Expand";
    }
}