package loki.utilitysystem;

import loki.CommunicationChannel;
import loki.data.UnitData;

/**
 * Created by Plankton on 2015-05-03.
 */
public class DefendBehaviour extends AbstractBehaviour {

    private CommunicationChannel commChannel;

    public DefendBehaviour(CommunicationChannel commChannel) {
        super(0.1);
        this.commChannel = commChannel;
    }

    @Override
    protected double calculateUtilityValue() {

        // TODO Defend not implemented yet, this just borrows attack code
        double ownArmyValue = 0;
        double enemyArmyValue = 0;

        for (UnitData u : commChannel.getUnitInventory().getOwnUnits()) {
            if (u.isMilitary()) {
                ownArmyValue += u.getUnit().getHitPoints();
            }
        }
        for (UnitData u : commChannel.getUnitInventory().getEnemyUnits()) {
            if (u.getType().canAttack()) {
                enemyArmyValue += u.getHitPoints();
            }
        }

        if (ownArmyValue == 0) {
            return 0;
        }
        return 0;
        //return ownArmyValue/(ownArmyValue+enemyArmyValue);
    }

    @Override
    public void onTerminate() {

    }

    public String toString() {
        return "Defend";
    }
}
