package loki.utilitysystem;

/**
 * Created by Plankton on 2015-04-19.
 */
public class DummyBehaviour extends AbstractBehaviour {

    private double equilibrium;
    private String behaviourName;

    public DummyBehaviour(double equilibrium, String name) {
        super(0.1);
        this.equilibrium = equilibrium;
        behaviourName = name;
    }

    @Override
    protected double calculateUtilityValue() {
        double fluctuation = Math.random()*0.2-0.1;
        return equilibrium + fluctuation;
    }

    @Override
    public void onTerminate() {

    }

    public String toString() {
        return behaviourName;
    }
}
