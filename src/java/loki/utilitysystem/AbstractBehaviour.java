package loki.utilitysystem;

/**
 * Created by Plankton on 2015-04-19.
 */
public abstract class AbstractBehaviour implements IBehaviour {

    protected double decayRate = -1;
    protected double utility;

    /**
     * Abstract class for a behaviour that handles updating the utility value by itself. Setting a decay rate larger
     * than 1 means that inertia will be used, otherwise the behaviour's utility will be exactly determined by the
     * method calculateUtilityValue().
     * @param decayRate Determines how quickly the utility value will converge to the value calculated by the
     *                  method calculateUtilityValue(). If decayRate < 0, no inertia will be used.
     */
    public AbstractBehaviour(double decayRate) {
        this.decayRate = decayRate;
    }

    /**
     * Calculates the value that the utility value will converge to.
     * @return
     */
    protected abstract double calculateUtilityValue();

    @Override
    public double getUtility() {
        return utility;
    }

    public void setUtility(double utilityValue) {
        this.utility = utilityValue;
    }

    @Override
    public void onInitialize() {
        utility = 1;
    }

    @Override
    public void tick() {
        if (decayRate > 0) { // use inertia
            double diff = calculateUtilityValue() - utility;
            utility += diff*decayRate;
        } else { // don't use inertia
            utility = calculateUtilityValue();
        }
    }
}
