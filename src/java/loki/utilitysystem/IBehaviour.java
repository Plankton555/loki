package loki.utilitysystem;

/**
 * Created by Plankton on 2015-04-19.
 */
public interface IBehaviour {

    void tick();

    double getUtility();

    void onInitialize();

    void onTerminate();
}
