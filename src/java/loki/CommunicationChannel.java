package loki;

import loki.data.UnitInventory;
import loki.managers.ArmyManager;
import loki.managers.ProductionManager;
import loki.managers.ScoutingManager;
import loki.managers.StrategyManager;

/**
 * Communication channel to allow interaction between different classes.
 * @author Plankton (2014-12-13)
 *
 */
public class CommunicationChannel {

    private ProductionManager productionManager;
    private ScoutingManager scoutingManager;
    private ArmyManager armyManager;
    private UnitInventory unitInventory;
    private StrategyManager strategyManager;


    public CommunicationChannel() {
    }

    public ProductionManager getProductionManager() {
        return productionManager;
    }

    public void setProductionManager(ProductionManager productionManager) {
        this.productionManager = productionManager;
    }

    public ScoutingManager getScoutingManager() {
        return scoutingManager;
    }

    public void setScoutingManager(ScoutingManager scoutingManager) {
        this.scoutingManager = scoutingManager;
    }

    public UnitInventory getUnitInventory() {
        return unitInventory;
    }

    public void setUnitInventory(UnitInventory unitInventory) {
        this.unitInventory = unitInventory;
    }

    public StrategyManager getStrategyManager() {
        return strategyManager;
    }

    public void setStrategyManager(StrategyManager strategyManager) {
        this.strategyManager = strategyManager;
    }

    public ArmyManager getArmyManager() {
        return armyManager;
    }

    public void setArmyManager(ArmyManager armyManager) {
        this.armyManager = armyManager;
    }
}
