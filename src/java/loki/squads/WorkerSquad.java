package loki.squads;

import bwapi.Mirror;
import bwapi.UnitType;
import bwta.BWTA;
import loki.Loki;
import loki.data.UnitData;
import loki.managers.AbstractEmployer;
import loki.managers.Base;
import loki.util.Utils;

/**
 * Created by Plankton on 2014-09-06.
 */
public class WorkerSquad extends AbstractEmployer {
	
	private Mirror bwapi;
	private Base base;

    public WorkerSquad(Mirror bwapi, Base base) {
        super(); // initializes attachedUnits
        this.bwapi = bwapi;
        this.base = base;
    }

    public void update() {
        super.update();
        // collect minerals
        for (UnitData ud : attachedUnits) {
            if (ud.getType() == UnitType.Zerg_Drone) {
                // You can use referential equality for units, too
            	if (!Utils.isSameRegionGroup(ud.getPosition(), base.getBaseBuilding().getPosition())) {
            		ud.rightClick(base.getBaseBuilding(), false);
            	}
                if (ud.isIdle()) {
                    double closestDistance = Double.MAX_VALUE;
                    UnitData closestMineral = null;
                    for (UnitData minerals : Loki.getInstance().getCommChannel().getUnitInventory().getNeutralUnits()) {
                        if (minerals.getType().isMineralField() && Utils.isSameRegionGroup(minerals.getPosition(), base.getBaseBuilding().getPosition())) {

                            double distance = ud.getDistance(minerals);
                            if (distance < closestDistance) {
                                closestDistance = distance;
                                closestMineral = minerals;
                            }
                        }
                    }
                    if (closestMineral != null) {
                        ud.rightClick(closestMineral, false);
                    }
                }
            } else { // if not worker
                detachUnit(ud); //unemploy
            }
        }
    }
}
