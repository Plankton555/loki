package loki.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;

import bwapi.Mirror;

public class DataMiner {

	// TODO add javadoc

	private static String delim = ";";
	private static String rowDelim = "\n";
	
	private Mirror mirror;
	
	private boolean winner;
	private String enemyRace = "unknown";
	private String enemyName;
	private String mapName;
	private int matchLength;
	
	public DataMiner(Mirror mirror) {
		this.mirror = mirror;
		System.out.println("Initializing data miner");
	}
	
	public void onStart() {
		// TODO Auto-generated method stub
		
	}

	public void onFrame() {
		// TODO Auto-generated method stub
		if (mirror.getGame().getFrameCount() % 100 == 0) {
			//System.out.println("Enemy race: " + mirror.getGame().enemy().getRace().c_str());
		}
	}

	public void onEnd(boolean winner) {
		this.winner = winner;
		String discoveredEnemyRace = Utils.getDiscoveredEnemyRace();
		this.enemyRace = discoveredEnemyRace == null ? "Unknown" : discoveredEnemyRace;
		this.enemyName = mirror.getGame().enemy().getName();
		this.mapName = mirror.getGame().mapFileName();
		this.matchLength = mirror.getGame().getFrameCount();
		
		if (Definitions.DATA_GATHERING_ACTIVE) {
			saveDataToCsvFile();
		}
	}
	
	private void saveDataToCsvFile() {
		// TODO add javadoc
		// First, find out if the file exists
		File file = new File(Definitions.DATA_MATCH_FILE + ".csv");
		File folder = file.getParentFile();

		//SimpleDateFormat monthFormat = new SimpleDateFormat("yyyy-MM");
		
		if (!folder.exists()) {
			folder.mkdirs();
		}
		boolean fileExists = file.exists();
		try {
			PrintWriter out = new PrintWriter(new OutputStreamWriter(new FileOutputStream(file, true), "UTF-8"));
			if (!fileExists) {
				// create new file and add header
					out.println(headerToString());
			}
			
			String dataString = dataToString();
			out.println(dataString);

			// append the data
			out.close();
		} catch (FileNotFoundException e) {
			// TODO handle exception
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO handle exception
			e.printStackTrace();
		}
		
		System.out.println("Saved data to file " + file.getAbsolutePath());
	}

	private String headerToString() {
		StringBuilder sb = new StringBuilder();

		sb.append("time").append(delim);
		sb.append("winner").append(delim);
		sb.append("map").append(delim);
		sb.append("enemyrace").append(delim);
		sb.append("enemyname").append(delim);
		sb.append("matchlength");
		
		return sb.toString();
	}

	private String dataToString() {
		SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
		Date now = new Date();
		String timeStamp = timeStampFormat.format(now);
		
		StringBuilder sb = new StringBuilder();

		sb.append(timeStamp).append(delim);
		sb.append(winner).append(delim);
		sb.append(mapName).append(delim);
		sb.append(enemyRace).append(delim);
		sb.append(enemyName).append(delim);
		sb.append(matchLength);
		
		return sb.toString();
	}

}
