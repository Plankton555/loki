package loki.util;

import java.util.List;

import bwapi.Color;
import bwapi.Mirror;
import bwapi.Position;
import bwapi.Unit;
import bwapi.Utils;
import bwta.BWTA;
import bwta.BaseLocation;
import loki.CommunicationChannel;
import loki.data.BuildOrderItem;
import loki.data.UnitData;
import loki.managers.Base;
import loki.utilitysystem.IBehaviour;
import loki.utilitysystem.UtilitySystem;

/**
 * Created by Plankton on 2014-09-06.
 */
public class DebugOverlay {

    private boolean DEBUG_MODE = false;

    public void drawDebugOverlay(Mirror bwapi, CommunicationChannel commChannel) {
        if (!DEBUG_MODE) {
            return;
        }
        for (Unit u : bwapi.getGame().getAllUnits()) {
            bwapi.getGame().drawCircleMap(u.getPosition().getX(), u.getPosition().getY(), 2, Color.Red);
        }

        printBuildOrder(bwapi, commChannel);
        printUnitInformation(bwapi, commChannel);
        printUtility(bwapi, commChannel);
        //printUnassignedUnits(bwapi, commChannel);
        printBaseInfo(bwapi, commChannel);
        printBaseLocations(bwapi, commChannel);
        printMatchInfo(bwapi, commChannel);
    }

	private void printUnassignedUnits(Mirror bwapi, CommunicationChannel commChannel) {
        List<UnitData> unassignedUnits = commChannel.getUnitInventory().getUnassignedUnits();

        int posY = 15;
        int posX = 450;
        bwapi.getGame().drawTextScreen(posX-3, posY, "Unassigned units:");
        posY += 10;
        for (UnitData u : unassignedUnits) {
            String text = u.getType().toString();
            bwapi.getGame().drawTextScreen(posX, posY, Utils.formatText(text, Utils.White));
            posY += 10;
        }
    }

    private void printUtility(Mirror bwapi, CommunicationChannel commChannel) {
        UtilitySystem utilitySystem = commChannel.getStrategyManager().getUtilitySystem();
        IBehaviour topBehaviour = utilitySystem.getTopBehaviour();

        int posY = 15;
        int posX = 280;
        bwapi.getGame().drawTextScreen(posX-3, posY, "Utility system:");
        posY += 10;
        for (IBehaviour b : utilitySystem.getBehaviours()) {
            String text = String.format("%.3f", b.getUtility()) + "  " + b.toString();
            if (b == topBehaviour) {
                bwapi.getGame().drawTextScreen(posX, posY, Utils.formatText(text, Utils.Green));
            } else {
                bwapi.getGame().drawTextScreen(posX, posY, Utils.formatText(text, Utils.White));
            }
            posY += 10;
        }
    }

    private void printUnitInformation(Mirror bwapi, CommunicationChannel commChannel) {
        int posY = 15;
        int posX = 150;
        bwapi.getGame().drawTextScreen(posX, posY, "# own units:   " + commChannel.getUnitInventory().getOwnUnits().size());
        posY += 10;
        bwapi.getGame().drawTextScreen(posX, posY, "# enemy units: " + commChannel.getUnitInventory().getEnemyUnits().size());
    }

    private void printBuildOrder(Mirror bwapi, CommunicationChannel commChannel) {
        int posY = 15;
        int posX = 10;
        bwapi.getGame().drawTextScreen(posX-3, posY, "Build order:");
        posY += 10;
        for (BuildOrderItem b : commChannel.getProductionManager().getBuildOrder().getBuildOrderItems()) {
            if (b.status == BuildOrderItem.BuildOrderStatus.ONGOING) {
                bwapi.getGame().drawTextScreen(posX, posY, Utils.formatText(b.toString(), Utils.Yellow));
            } else {
                bwapi.getGame().drawTextScreen(posX, posY, Utils.formatText(b.toString(), Utils.White));
            }
            posY += 10;
            
            if (b.unitType.isBuilding() && b.preferredPosition != null) {
            	int mapPosX = b.preferredPosition.getX()*32;
            	int mapPosY = b.preferredPosition.getY()*32;
            	int top = mapPosY;
            	int bottom = mapPosY + b.unitType.tileHeight()*32;
            	int left = mapPosX;
            	int right = mapPosX + b.unitType.tileWidth()*32;
            	bwapi.getGame().drawBoxMap(left, top, right, bottom, Color.White);
            }
        }
    }

    private void printBaseInfo(Mirror bwapi, CommunicationChannel commChannel) {
		for (Base base : commChannel.getProductionManager().getBases()) {
			Position pos = base.getBaseBuilding().getPosition();
			int nrWorkers = base.getWorkers().getNrEmployees();
			int nrWanted = base.getNrWorkersWanted();
			int relative = base.getRelativeNrNeededWorkers();
			bwapi.getGame().drawTextMap(pos.getX()-15, pos.getY()+10, Utils.formatText("W: " + nrWorkers + "/" + nrWanted + " -> " + relative, Utils.Cyan));
			bwapi.getGame().drawCircleMap(pos.getX(), pos.getY(), base.mineralDistanceThreshold, Color.Yellow);
		}
	}

	private void printBaseLocations(Mirror bwapi, CommunicationChannel commChannel) {
		List<BaseLocation> baseLocations = BWTA.getBaseLocations();
		for (BaseLocation baseLoc : baseLocations) {
			Position pos = baseLoc.getPosition();
			bwapi.getGame().drawCircleMap(pos.getX(), pos.getY(), 50, Color.Red);
		}
	}

	private void printMatchInfo(Mirror bwapi, CommunicationChannel commChannel) {
        int posY = 20;
        int posX = 450;
        bwapi.getGame().drawTextScreen(posX, posY, "Map:   " + bwapi.getGame().mapFileName());
        posY += 10;
        bwapi.getGame().drawTextScreen(posX, posY, "Frame: " + bwapi.getGame().getFrameCount());
	}
}
