package loki.util;

import bwapi.Game;
import bwapi.Position;
import loki.Loki;

public class Utils {
	
	private static String discoveredEnemyRace = null;

	public static boolean isSameRegionGroup(Position position1, Position position2) {
		// TODO add javadoc
		// TODO does not seem to work... debug...
		Game game = Loki.getInstance().getGame();
		return game.getRegionAt(position1).getRegionGroupID() == game.getRegionAt(position2).getRegionGroupID();
	}
	
	public static String getDiscoveredEnemyRace() {
		// TODO add javadoc
		return discoveredEnemyRace;
	}
	
	public static void setDiscoveredEnemyRace(String enemyRace) {
		// TODO add javadoc
		discoveredEnemyRace = enemyRace;
	}

}
